$('.card-action').on('click', '.card-action__btn', function(event) {
    $(event.delegateTarget)
        .toggleClass('card-action_state_active')
        .find('.tab-pane')
        .toggleClass('active');
    event.preventDefault();
});
