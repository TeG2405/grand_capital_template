$(document).on('click', '.counter-group', function(event) {
    if (event.target.innerText.indexOf('+') + 1) {
        Array.from(event.currentTarget.getElementsByClassName('counter__input')).forEach(function(input) {
            input.value =!input.value ? Number(input.placeholder) * 2 : Number(input.value) + Number(input.placeholder);
        });
    }
    if (event.target.innerText.indexOf('-') + 1) {
        Array.from(event.currentTarget.getElementsByClassName('counter__input')).forEach(function(input) {
            input.value =!input.value ? Number(input.placeholder) : Number(input.value) - Number(input.placeholder);
        });
    }
});
