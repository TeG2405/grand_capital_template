module.exports = function(bh) {
    bh.match('counter__input', function(ctx, json) {
        ctx.tag('input').attrs({
            type: 'text',
            placeholder: '1 000 000',
        });
    });
};
