module.exports = function(bh) {
    bh.match('card-action-table__collapse', function(ctx, json) {
        ctx.tParam('ID', ctx.generateId());
        ctx.cls('collapse').attrs({
            id: ctx.tParam('ID'),
        });
    });
};
