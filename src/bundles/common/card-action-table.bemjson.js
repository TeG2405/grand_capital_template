const ITEM = [
    {elem: 'item', content: [
        {elem: 'row', cls: 'row align-items-center', content: [
            {elem: 'col', cls: 'col-12 col-xl-4', content: [
                {elem: 'title', content: 'Деринат р-р д/в/м введ. 15 мг/м 5мл фл. №15/р-р д/в/м введ. 15 мг/м 5мл фл.'},
                {elem: 'code', cls: 'mt-x', content: 'код: 123456'},
            ]},
            {elem: 'col', cls: 'col-12 col-xl-8', content: [
                {elem: 'row', cls: 'row align-items-center', content: [
                    {elem: 'col', cls: 'col-12 col-xl-2-10', attrs: {'data-label': 'Учетная цена'}, content: [
                        {elem: 'group', content: [
                            {elem: 'price', content: '859 р'},
                        ]},
                    ]},
                    {elem: 'col', cls: 'col-12 col-md-4 col-xl-3-10', attrs: {'data-label': 'План закупки'}, content: [
                        {elem: 'group', content: [
                            {elem: 'count', content: '100 уп.'},
                            {elem: 'price', content: '859 000 000 р'},
                        ]},
                    ]},
                    {elem: 'col', cls: 'col-12 col-md-8 col-xl-6', attrs: {'data-label': 'Расчитайте свою выгоду'}, content: [
                        {elem: 'row', mix: {block: 'counter-group'}, cls: 'row align-items-center', content: [
                            {elem: 'col', cls: 'col-12 col-sm-6 col-xl-12', content: [
                                {block: 'counter', content: [
                                    {elem: 'inner', content: [
                                        {elem: 'btn', content: '-'},
                                        {elem: 'input', attrs: {placeholder: '100'}},
                                        {elem: 'btn', content: '+'},
                                    ]},
                                    {elem: 'append', content: 'уп.'},
                                ]},
                            ]},
                            {elem: 'col', cls: 'col-12 col-sm-6 col-xl-12', content: [
                                {block: 'counter', content: [
                                    {elem: 'inner', content: [
                                        {elem: 'btn', content: '-'},
                                        {elem: 'input', attrs: {placeholder: '859000000'}},
                                        {elem: 'btn', content: '+'},
                                    ]},
                                    {elem: 'append', content: 'Р'},
                                ]},
                            ]},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
module.exports = [
    {block: 'card-action-table', content: [
        {elem: 'header', cls: 'd-none d-xl-block', content: [
            {elem: 'row', cls: 'row', content: [
                {elem: 'col', cls: 'col-xl-4', content: 'Название товара'},
                {elem: 'col', cls: 'col-xl-8', content: [
                    {elem: 'row', cls: 'row', content: [
                        {elem: 'col', cls: 'col-12 col-xl-2-10', content: 'Учетная цена'},
                        {elem: 'col', cls: 'col-12 col-xl-3-10', content: 'План закупки'},
                        {elem: 'col', cls: 'col-12 col-xl-6', content: 'Расчитайте свою выгоду'},
                    ]},
                ]},
            ]},
        ]},
        {elem: 'body', content: [
            new Array(3).fill(ITEM),
            {elem: 'collapse', content: [
                new Array(7).fill(ITEM),
            ]},
        ]},
    ]},
];
