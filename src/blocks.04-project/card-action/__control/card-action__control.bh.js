module.exports = function(bh) {
    bh.match('card-action__control', function(ctx, json) {
        ctx.tParam('ID', ctx.generateId());
        ctx.cls('collapsed')
            .attrs({
                'data-target': '#'+ctx.tParam('ID'),
                'data-toggle': 'collapse',
            })
            .content([
                {tag: 'span', content: 'Показать еще'},
                {tag: 'span', content: 'Свернуть'},
            ]);
    });
};
