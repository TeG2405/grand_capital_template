module.exports = {
  block: 'page',
  title: 'Блоки для lk-gc',
  content: [
    require('./common/header.bemjson.js'),
    {mix: {block: 'container'}, content: [
      {block: 'row', content: [
        {block: 'col-12', cls: 'col-lg-3 col-xl-2'},
        {block: 'col-12', cls: 'col-lg-9 col-xl-10', content: [
          {block: 'pl-lg-3', content: [
            new Array(4).fill(require('./common/card-action.bemjson')),
          ]},
        ]},
      ]},
    ]},
    require('./common/footer.bemjson.js'),
  ],
};
