const ITEM = [
    {elem: 'item', content: [
        {elem: 'row', cls: 'row align-items-center', content: [
            {elem: 'col', cls: 'col-12 col-xl-4', content: [
                {elem: 'title', content: 'Деринат р-р д/в/м введ. 15 мг/м 5мл фл. №15/р-р д/в/м введ. 15 мг/м 5мл фл.'},
                {elem: 'code', cls: 'mt-x', content: 'код: 123456'},
            ]},
            {elem: 'col', cls: 'col-12 col-xl-8', content: [
                {elem: 'row', cls: 'row align-items-center', content: [
                    {elem: 'col', cls: 'col-12 col-xl-2-10', attrs: {'data-label': 'Учетная цена'}, content: [
                        {elem: 'group', content: [
                            {elem: 'price', content: '859 р'},
                        ]},
                    ]},
                    {elem: 'col', cls: 'col-12 col-md-4 col-xl-3-10', attrs: {'data-label': 'План закупки'}, content: [
                        {elem: 'group', content: [
                            {elem: 'count', content: '100 уп.'},
                            {elem: 'price', content: '859 000 000 р'},
                        ]},
                    ]},
                    {elem: 'col', cls: 'col-12 col-md-6 col-xl-5', attrs: {'data-label': 'Фактические закупки'}, content: [
                        {elem: 'group', content: [
                            {elem: 'count', content: '100 уп.'},
                            {elem: 'price', content: '859 000 000 р'},
                        ]},
                    ]},
                    {elem: 'col', mix: {elem: 'control'}, cls: 'col-12 col-md-2', content: [
                        {elem: 'btn', content: [
                            {block: 'fi', mods: {icon: 'basket'}},
                            {tag: 'span', cls: 'd-md-none ml-s', content: 'В корзину'},
                        ]},
                    ]},
                    {elem: 'col', cls: 'col-12 col-xl-11', content: [
                        {cls: 'd-flex align-items-center', content: [
                            {cls: 'flex-grow-1 pr-m', content: [
                                {elem: 'progress', attrs: {style: `left: 100%`}},
                            ]},
                            {elem: 'count', content: '100 %'},
                        ]},
                    ]},
                ]},
            ]},
        ]},
    ]},
];
module.exports = [
    {block: 'card-action-table', content: [
        {elem: 'header', cls: 'd-none d-xl-block', content: [
            {elem: 'row', cls: 'row', content: [
                {elem: 'col', cls: 'col-xl-4', content: 'Название товара'},
                {elem: 'col', cls: 'col-xl-8', content: [
                    {elem: 'row', cls: 'row', content: [
                        {elem: 'col', cls: 'col-12 col-xl-2-10', content: 'Учетная цена'},
                        {elem: 'col', cls: 'col-12 col-xl-3-10', content: 'План закупки'},
                        {elem: 'col', cls: 'col-12 col-xl-6', content: 'Фактические закупки'},
                    ]},
                ]},
            ]},
        ]},
        {elem: 'body', content: [
            new Array(3).fill(ITEM),
            {elem: 'collapse', content: [
                new Array(7).fill(ITEM),
            ]},
        ]},
    ]},
];
