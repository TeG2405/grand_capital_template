module.exports = [
    {block: 'card-action', content: [
        {elem: 'row', cls: 'row align-items-end', content: [
            {elem: 'col', cls: 'col-12 col-sm-6 col-xl-4', content: [
                {elem: 'header', content: [
                    {elem: 'title', content: '№ 234-567-89'},
                    {elem: 'badge', cls: 'badge', content: 'Новая'},
                ]},
                {elem: 'date', content: '14.08.2017 — 21.10.2018'},
                {elem: 'remark', content: '(осталось 10 дней)'},
            ]},
            {elem: 'col', cls: 'col-6 col-xl-4 order-2', content: [
                {elem: 'label', content: 'Плановый бонус'},
                {elem: 'bonus', content: '200 000 <span class="text-purple">(10%)</span>'},
            ]},
            {elem: 'col', cls: 'col-12 col-sm-12 col-xl-4 order-2 order-sm-1 order-xl-2', content: [
                {elem: 'btn', content: [
                    {block: 'fi', mods: {icon: 'heart'}},
                    {content: 'Участвовать в акции'},
                ]},
                {elem: 'remark', cls: 'mt-m', content: '10 аптек уже зарегистрировались'},
            ]},
        ]},
        {elem: 'body', content: [
            {block: 'tab-content', content: [
                {block: 'tab-pane', cls: 'active', content: [
                    require('./card-action-table.bemjson'),
                ]},
                {block: 'tab-pane', content: [
                    require('./card-action-table-active.bemjson'),
                ]},
            ]},
        ]},
        {elem: 'control'},
        {elem: 'row', cls: 'row', content: [
            {elem: 'col', cls: 'col-12 col-xl-8 order-xl-1', content: [
                {elem: 'row', cls: 'row', content: [
                    {elem: 'col', cls: 'col-12 col-xl-2-10 d-xl-block'},
                    {elem: 'col', cls: 'col-12 col-sm-6 col-md-4 col-xl-3-10', content: [
                        {elem: 'label', content: 'Плановый бонус'},
                        {elem: 'bonus', elemMods: {size: 'sm'}, content: '200 000 <span class="text-purple">(10%)</span>'},
                    ]},
                    {elem: 'col', cls: 'col-8 col-sm-6 col-md-4 col-xl-6', content: [
                        {elem: 'label', content: 'Фактический бонус'},
                        {elem: 'bonus', elemMods: {size: 'sm'}, content: '200 000 <span class="text-purple">(10%)</span>'},
                    ]},
                ]},
            ]},
            {elem: 'col', cls: 'col-12 col-xl-4', content: [
                {elem: 'label', content: 'Все товары, участвующие в акции'},
                {elem: 'bonus', elemMods: {size: 'sm'}, content: '<span class="text-purple">10</span>'},
            ]},
        ]},
    ]},
];
